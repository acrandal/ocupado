/**
 *
 * Ocupado (occupied) sign for our local bathroom
 * 
 *  Uses an EM deflection sensor to determine occupancy
 *  Uses a Sharp rangefinder to detect door open/close
 *  Sign lit up by NeoPixels
 *  Feedback to bathroom user via a single LED
 *
 * Contributors:
 *  Aaron S. Crandall <acrandal@gmail.com>, 2019
 *  
 * License: 
 *
 * Examples and notes from these sources:
 * https://www.arduino.cc/reference/en/language/functions/analog-io/analogread/
 *  Arduino Uno with 5v reference, analog read to volts:
 *   volts = analogRead(pin) * 0.0049
 * 
 * https://github.com/zoubworldArduino/ZSharpIR/
 *  Used README.md from ZSharpIR for range curve fitting power function and scaling values
 *  Sharp GP2Y0A21Y range finder (10 - 80cm)
 *  Distance (cm) = 29.988 X POW(Volt, -1.173)
 *  float distance = 29.988 * pow(volts, -1.173)
 */


int sensorpin = A0;             // analog pin used to connect the sharp sensor
int val = 0;                    // variable to store the values from sensor(initially zero)

void setup()
{
  Serial.begin(115200);         // starts the serial monitor
  delay(100);                   // Delay to let serial initialize
  Serial.println("Ready.");
}
 
void loop()
{
  val = analogRead(sensorpin);       // reads the value of the sharp sensor
  //Serial.println(val);            // prints the value of the sensor to the serial monitor
  float volts = val * 0.0049;

  float distance = 29.988 * pow(volts, -1.173);
  Serial.println(distance);
  delay(10);                    // wait for this much time before printing next value
}
